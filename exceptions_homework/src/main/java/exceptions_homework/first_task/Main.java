package exceptions_homework.first_task;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        try {
            System.out.println("Enter first number:");
            int a = num.nextInt();

            System.out.println("Enter second number:");
            int b = num.nextInt();

            if (a < 0 || b < 0)
                throw new IllegalArgumentException("Negative number is not allowed");
            System.out.println("The area of the rectangle is " + (a * b));

        } catch(InputMismatchException e){
            System.err.println("Nonnumeric value");
        }
    }
}