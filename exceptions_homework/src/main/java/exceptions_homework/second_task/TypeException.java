package exceptions_homework.second_task;

public class TypeException extends Exception {
    public TypeException(String message) {
        super(message);
    }

    public TypeException() {
    }
}