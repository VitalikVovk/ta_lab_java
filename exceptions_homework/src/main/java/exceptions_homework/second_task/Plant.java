package exceptions_homework.second_task;

public class Plant {
    private Type type;
    private Color color;
    private int size;

    public Plant(Type type, Color color, int size) {
        this.type = type;
        this.color = color;
        this.size = size;
    }
    

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Plant{" +
                "type=" + type +
                ", color=" + color +
                ", size=" + size +
                '}';
    }

}
