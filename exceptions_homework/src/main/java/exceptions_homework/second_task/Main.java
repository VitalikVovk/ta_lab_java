package exceptions_homework.second_task;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Plant> plantList;
        plantList = Arrays.asList(new Plant(Type.HOME_TYPE, Color.BLUE, 10),
                new Plant(Type.HOME_TYPE, Color.RED, 20),
                new Plant(Type.HOME_TYPE, Color.WHITE, 30),
                new Plant(Type.HOME_TYPE, Color.BLACK, 40),
                new Plant(Type.WILD_TYPE, Color.RED, 50));

        System.out.println("List of plants:");
        for (Plant plant : plantList) {
            System.out.println(plant);
        }
        try {
            for (Plant plant : plantList) {
                if (plant.getColor().equals(Color.WHITE)) {
                    throw new ColorException("Wrong Color");
                }
                if (plant.getType().equals(Type.WILD_TYPE)) {
                    throw new TypeException("Wrong Type");
                }
            }
        } catch (ColorException | TypeException e) {
            e.printStackTrace();
        }
    }
}