package exceptions_homework.second_task;

public class ColorException extends Exception {
    public ColorException(String message) {
        super(message);
    }

    public ColorException() {
    }
}