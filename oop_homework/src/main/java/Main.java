import java.util.Scanner;

import flowerpot.*;
import flower.*;

public class Main {
    public static void main(String[] args) {

        Flowerpot flowerpot = null;
        Flower flower = null;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to FlowerShop");
        System.out.println("What flower do you want?");
        System.out.println("1 - Orchid, 2 - Rose, 3 - Tulip, 4 - Sunflower");

        int number = Integer.parseInt(scanner.nextLine());

        switch (number) {
            case 1:
                flower = new Orchid();
                break;
            case 2:
                flower = new Rose();
                break;
            case 3:
                flower = new Tulip();
                break;
            case 4:
                flower = new Sunflower();
                break;
        }

        System.out.println("What flowerpot do you want?");
        System.out.println("1 - American, 2 - Asian, 3 - European");

        number = Integer.parseInt(scanner.nextLine());
        switch (number) {
            case 1:
                flowerpot = new AmericanFlowerpot();
                break;
            case 2:
                flowerpot = new AsianFlowerpot();
                break;
            case 3:
                flowerpot = new EuropeanFlowerpot();
                break;
        }

        System.out.println("Your order is");
        System.out.println(flower.toString());
        System.out.println(flowerpot.toString());
        System.out.println("It will cost you - one smile");
    }
}
